using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.DataModels;

namespace tech_test_payment_api.Context
{
  public class PaymentContext : DbContext
  {

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseInMemoryDatabase(databaseName: "PaymentAPI");
    }

    public DbSet<VendedorDataModel> Vendedores { get; set; }
  }
}
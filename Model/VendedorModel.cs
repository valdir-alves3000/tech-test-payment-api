namespace tech_test_payment_api.Model
{
  public class VendedorModel
  {
    public string Nome { get; set; }
    public string Email { get; set; }
    public string CPF { get; set; }
    public string Telefone { get; set; }
  }
}
namespace tech_test_payment_api.Enums
{
  public enum EStatus
  {
    AGUARDANDO_PAGAMENTO,
    PAGAMENTO_APROVADO,
    ENVIADO_PARA_TRANSPORTADORA,
    ENTREGUE,
    CANCELADA
  }
}
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.DataModels
{
  public class ItemDataModel
  {
    [Key]
    public Guid Id { get; set; }
    public string Nome { get; set; }
    public decimal Preco { get; set; }

  }
}
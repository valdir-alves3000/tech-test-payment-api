using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.DataModels
{
  public class VendedorDataModel
  {
    [Key]
    public Guid Id { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string CPF { get; set; }
    public string Telefone { get; set; }

  }
}
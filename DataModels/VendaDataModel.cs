using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.DataModels
{
  public class VendaDataModel
  {
    [Key]
    public Guid Id { get; set; }
    public DateTime Data { get; set; }
    public EStatus Status { get; set; }
    public VendedorDataModel Vendedor { get; set; }

    public List<ItemDataModel> Itens { get; set; }
  }
}
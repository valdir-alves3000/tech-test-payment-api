using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.DataModels;

namespace tech_test_payment_api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ItemController : ControllerBase
  {

    private readonly PaymentContext _context;
    public ItemController(PaymentContext context) => _context = context;

    [HttpGet("GetList")]
    public IActionResult GetList()
    {
      return Ok(_context.Itens.ToList());
    }

    [HttpGet]
    public IActionResult GetById(Guid id)
    {
      ItemDataModel item = _context.Itens.Find(id);
      if (item == null) return NotFound();

      return Ok(item);
    }

    [HttpPost]
    public IActionResult Create([Required] string Nome, [Required] int Preco)
    {
      if (Nome.Length <= 3)
        return BadRequest(new { Erro = "Nome Obrigário, minímo quatro caracteres!" });

      ItemDataModel item = new ItemDataModel();
      item.Id = Guid.NewGuid();
      item.Nome = Nome;
      item.Preco = Preco;

      _context.Itens.Add(item);
      _context.SaveChanges();

      return Ok(item);
    }
  }
}
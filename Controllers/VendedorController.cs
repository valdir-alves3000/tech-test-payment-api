using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.DataModels;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
public class VendedorController : ControllerBase
{
  private readonly PaymentContext _context;

  public VendedorController(PaymentContext context) => _context = context;

  [HttpGet("GetList")]
  public IActionResult GetList()
  {
    return Ok(_context.Vendedores.ToList());
  }
  [HttpGet]
  public IActionResult GetById(Guid id)
  {
    VendedorDataModel vendedor = _context.Vendedores.Find(id);

    if (vendedor == null) return NotFound();

    return Ok(vendedor);
  }

  [HttpPost]
  public IActionResult Create([Required] string Nome, [Required] string Email, [Required] string CPF, [Required] string Telefone)
  {
    bool isvalid = Nome.Length > 3 && Email.Contains("@");

    if (!isvalid)
      return BadRequest(new { Erro = "Todo campo é Obrigatório!" });

    VendedorDataModel vendedor = new VendedorDataModel();
    vendedor.Id = Guid.NewGuid();
    vendedor.Nome = Nome;
    vendedor.Email = Email;
    vendedor.CPF = CPF;
    vendedor.Telefone = Telefone;

    _context.Vendedores.Add(vendedor);
    _context.SaveChanges();

    return Ok(vendedor);
  }

}

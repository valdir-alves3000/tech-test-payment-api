using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.DataModels;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class VendaController : ControllerBase
  {
    private readonly PaymentContext _context;
    public VendaController(PaymentContext context) => _context = context;

    [HttpGet("GetList")]
    public IActionResult GetList()
    {
      return Ok(_context.Vendas.ToList());
    }

    [HttpGet]
    public IActionResult GetById(Guid id)
    {
      VendaDataModel venda = _context.Vendas.Find(id);
      if (venda == null) return NotFound();

      return Ok(venda);
    }

    [HttpPost]
    public IActionResult Create([Required] Guid vendedorId, [Required] List<Guid> itensId)
    {

      VendaDataModel venda = new VendaDataModel();
      var vendedor = _context.Vendedores.Find(vendedorId);
      if (vendedor == null) return BadRequest(new { erro = "Vendedor não Localizado!" });


      if (itensId.Count() < 1) return BadRequest(new { erro = "Sua compra precisa ter pelo menos um item" });

      List<ItemDataModel> itens = new List<ItemDataModel>();

      foreach (var item in itensId)
      {
        itens.Add(_context.Itens.Find(item));
      }


      venda.Id = Guid.NewGuid();
      venda.Vendedor = vendedor;
      venda.Status = EStatus.AGUARDANDO_PAGAMENTO;
      venda.Itens = itens;
      venda.Data = DateTime.Now;

      _context.Vendas.Add(venda);
      _context.SaveChanges();

      return Ok(venda);
    }

    [HttpPatch("Status")]
    public IActionResult Update([Required] Guid id, [Required] EStatus status)
    {
      VendaDataModel venda = _context.Vendas.Find(id);
      if (venda == null) return NotFound();

      switch (venda.Status)
      {
        case EStatus.AGUARDANDO_PAGAMENTO:
          if (status != EStatus.PAGAMENTO_APROVADO && status != EStatus.CANCELADA)
            return BadRequest(new { erro = "Status Inválido" });
          break;


        case EStatus.PAGAMENTO_APROVADO:
          if (status != EStatus.ENVIADO_PARA_TRANSPORTADORA && status != EStatus.CANCELADA)
            return BadRequest(new { erro = "Status Inválido" });
          break;

        case EStatus.ENVIADO_PARA_TRANSPORTADORA:
          if (status != EStatus.ENTREGUE)
            return BadRequest(new { erro = "Status Inválido" });
          break;

        default:
          return BadRequest(new { erro = "Status Inválido" });

      }

      venda.Status = status;
      _context.Vendas.Update(venda);
      _context.SaveChanges();

      return Ok(venda);
    }
  }
}